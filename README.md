## Docker practice Application

### How to run it?

An entire application can be ran with a single command in a terminal:

```
$ docker-compose up -d
```

If you want to stop it use following command:

```
$ docker-compose down
```

Full list of available REST endpoints could be found in Swagger UI,
which could be called using link: **http://localhost:8080/swagger-ui.html** or **http://192.168.99.100:8080/swagger-ui.html**
