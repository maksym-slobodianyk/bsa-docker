INSERT INTO user_roles(id, name) VALUES ('256afa3d-b9a7-485e-8504-0edab500ff4a','admin');
INSERT INTO user_roles(id, name) VALUES ('8c9f93be-2dac-48de-971e-688085efb198','user');
INSERT INTO user_roles(id, name) VALUES ('185b2126-cd08-4f74-969b-c1325c02078c','tech');

INSERT INTO users(id, name, avatar, role_id,email,password) VALUES ('bd5ea827-64ff-471c-a5e2-4b4b15dd300c','user','https://i.imgur.com/OE6SAt3.jpg','256afa3d-b9a7-485e-8504-0edab500ff4a','user@bsa.com','$2y$10$cgJReEGcQL8fyN6HNjgAg.RQ9l2HEaJcoyzQLutbjSJKOefQVAnp6');
INSERT INTO users(id, name, avatar, role_id,email,password) VALUES ('dfe2f5fd-1a6e-456e-8878-81b1162d8356','admin','https://i.imgur.com/OE6SAt3.jpg','185b2126-cd08-4f74-969b-c1325c02078c','admin@bsa.com','$2y$10$mD4/Lqh6HQu3O75G0UqxRu7JLSX9Ygu38ouDUGgF8KwCLtbTsMtZS');
