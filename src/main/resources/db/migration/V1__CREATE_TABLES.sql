
-------------------------------------------------------------------
---------------------------ROLES-----------------------------------
-------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS user_roles
(
    id   uuid PRIMARY KEY,
    name varchar UNIQUE NOT NULL
);

-------------------------------------------------------------------
---------------------------USERS-----------------------------------
-------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS users
(
    id       uuid PRIMARY KEY,
    name     varchar UNIQUE NOT NULL,
    avatar   varchar        NOT NULL,
    role_id  uuid           NOT NULL,
    email    varchar UNIQUE NOT NULL,
    password varchar        NOT NULL,
    FOREIGN KEY (role_id) REFERENCES user_roles (id)
);


-------------------------------------------------------------------
--------------------------MESSAGES---------------------------------
-------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS messages
(
    id         uuid PRIMARY KEY,
    text       varchar   NOT NULL,
    created_at timestamp NOT NULL,
    updated_on timestamp,
    user_id    uuid      NOT NULL,
    FOREIGN KEY (user_id) REFERENCES users (id)
);


-------------------------------------------------------------------
---------------------MESSAGE_REACTIONS-----------------------------
-------------------------------------------------------------------
CREATE TABLE IF NOT EXISTS message_reactions
(
    id         uuid PRIMARY KEY,
    message_id uuid NOT NULL,
    user_id    uuid NOT NULL,
    FOREIGN KEY (message_id) REFERENCES messages (id),
    FOREIGN KEY (user_id) REFERENCES users (id)
);

ALTER TABLE message_reactions
    ADD CONSTRAINT  message_user_uniqueness UNIQUE (message_id, user_id)