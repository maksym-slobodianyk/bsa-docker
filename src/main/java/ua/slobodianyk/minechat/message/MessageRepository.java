package ua.slobodianyk.minechat.message;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ua.slobodianyk.minechat.message.dto.MessageDto;
import ua.slobodianyk.minechat.message.model.Message;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface MessageRepository extends CrudRepository<Message, UUID> {
    @Query("SELECT new ua.slobodianyk.minechat.message.dto.MessageDto(m.id, " +
            "m.text, " +
            "m.createdAt, " +
            "m.updatedOn, " +
            "u.id, " +
            "u.avatar, " +
            "u.name, " +
            "(SELECT true FROM MessageReaction r WHERE r.user.id = :userId AND r.message.id = m.id)) " +
            "FROM Message m LEFT JOIN m.user u order by m.createdAt")
    List<MessageDto> findAllMessages(UUID userId);

    @Query("SELECT new ua.slobodianyk.minechat.message.dto.MessageDto(m.id, " +
            "m.text, " +
            "m.createdAt, " +
            "m.updatedOn, " +
            "u.id, " +
            "u.avatar, " +
            "u.name, " +
            "(SELECT true FROM MessageReaction r WHERE r.user.id = :userId AND r.message.id = m.id)) " +
            "FROM Message m LEFT JOIN m.user u WHERE m.id = :messageId  order by m.createdAt")
    Optional<MessageDto> findMessageById(UUID messageId, UUID userId);
}
