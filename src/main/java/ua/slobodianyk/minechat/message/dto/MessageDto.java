package ua.slobodianyk.minechat.message.dto;


import lombok.*;

import java.util.Date;
import java.util.UUID;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class MessageDto {

    private UUID id;
    private String text;
    private Date createdAt;
    private Date updatedAt;
    private UUID userId;
    private String avatar;
    private String user;
    private Boolean isReacted;

}
