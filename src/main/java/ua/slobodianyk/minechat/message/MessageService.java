package ua.slobodianyk.minechat.message;

import org.springframework.stereotype.Service;
import ua.slobodianyk.minechat.message.dto.MessageCreationDto;
import ua.slobodianyk.minechat.message.dto.MessageDto;
import ua.slobodianyk.minechat.message.dto.MessageUpdateDto;
import ua.slobodianyk.minechat.message.model.Message;
import ua.slobodianyk.minechat.user.model.User;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class MessageService {

    private MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public List<MessageDto> getAll(UUID userId) {
        return messageRepository.findAllMessages(userId);
    }

    public MessageDto getById(UUID messageId, UUID userId) {
        return messageRepository.findMessageById(messageId, userId).orElseThrow(IllegalArgumentException::new);
    }

    public void create(MessageCreationDto messageDTO) {
        messageRepository.save(Message.builder()
                .text(messageDTO.getText())
                .user(User.builder()
                        .id(messageDTO.getUserId())
                        .build())
                .createdAt(new Date())
                .build());
    }

    public void update(MessageUpdateDto messageDTO) {

        Message messageToUpdate = messageRepository
                .findById(messageDTO.getId())
                .orElseThrow(IllegalArgumentException::new);

        messageToUpdate.setUpdatedOn(new Date());
        messageToUpdate.setText(messageDTO.getText());

        messageRepository.save(messageToUpdate);
    }

    public void delete(UUID messageID) {
        messageRepository.deleteById(messageID);
    }


}
