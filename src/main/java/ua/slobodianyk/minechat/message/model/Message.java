package ua.slobodianyk.minechat.message.model;


import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import ua.slobodianyk.minechat.messageReaction.model.MessageReaction;
import ua.slobodianyk.minechat.user.model.User;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "messages")

public class Message {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    private UUID id;

    @Column
    private String text;

    @Column(name = "created_at")
    private Date createdAt;

    @Column(name = "updated_on")
    private Date updatedOn;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "message", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<MessageReaction> reactions;
}
