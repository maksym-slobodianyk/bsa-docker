package ua.slobodianyk.minechat.messageReaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import ua.slobodianyk.minechat.message.model.Message;
import ua.slobodianyk.minechat.messageReaction.dto.ReactionDto;
import ua.slobodianyk.minechat.messageReaction.model.MessageReaction;
import ua.slobodianyk.minechat.user.model.User;

import javax.transaction.Transactional;


@Service
@Transactional
public class MessageReactionService {

    private ReactionRepository reactionRepository;

    @Autowired
    public MessageReactionService(ReactionRepository reactionRepository) {
        this.reactionRepository = reactionRepository;
    }

    public void toggleReaction(@RequestBody ReactionDto reaction) {
        if (reactionRepository.findByMessageIdAndUserId(reaction.getMessageId(), reaction.getUserId()).isPresent())
            reactionRepository.deleteByMessageIdAndUserId(reaction.getMessageId(), reaction.getUserId());
        else
            reactionRepository.save(MessageReaction.builder()
                    .user(User.builder().id(reaction.getUserId()).build())
                    .message(Message.builder().id(reaction.getMessageId()).build())
                    .build());
    }
}
