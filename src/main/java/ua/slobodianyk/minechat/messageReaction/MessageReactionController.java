package ua.slobodianyk.minechat.messageReaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.slobodianyk.minechat.messageReaction.dto.ReactionDto;
import ua.slobodianyk.minechat.messageReaction.model.MessageReaction;
import ua.slobodianyk.minechat.userRole.model.UserRole;

import java.util.List;

@RestController
@RequestMapping("/api/reactions")
public class MessageReactionController {

    private MessageReactionService messageReactionService;

    @Autowired
    public MessageReactionController(MessageReactionService messageReactionService) {
        this.messageReactionService = messageReactionService;
    }

    @PutMapping
    public void toggleReaction(@RequestBody ReactionDto reaction) {
        messageReactionService.toggleReaction(reaction);
    }
}
