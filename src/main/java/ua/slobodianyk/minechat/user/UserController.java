package ua.slobodianyk.minechat.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ua.slobodianyk.minechat.user.dto.UserDto;
import ua.slobodianyk.minechat.user.dto.UserUpdateDto;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/users")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    public UserController() {
    }

    @GetMapping("/all")
    public List<UserDto> getAll() {
        return userService.getAll();
    }


    @GetMapping
    public UserDto getById(@RequestParam UUID userId) throws ChangeSetPersister.NotFoundException {
        return userService.getById(userId);
    }




    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public void update(@RequestBody UserUpdateDto updateDto) throws ChangeSetPersister.NotFoundException {
        userService.update(updateDto);
    }

    @DeleteMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@RequestParam UUID userId) {
        userService.deleteById(userId);
    }
}
