package ua.slobodianyk.minechat.auth;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ua.slobodianyk.minechat.auth.dto.AuthUserDTO;
import ua.slobodianyk.minechat.auth.dto.UserLoginDTO;
import ua.slobodianyk.minechat.auth.dto.UserRegisterDto;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    @Autowired
    private AuthService authService;


    @PostMapping("/register")
    public AuthUserDTO signUp(@RequestBody UserRegisterDto user) throws Exception {
        return authService.register(user);
    }

    @PostMapping("/login")
    public AuthUserDTO login(@RequestBody UserLoginDTO user) throws Exception {
        AuthUserDTO dti = authService.login(user);
        System.out.println("after\n\n\n");
        return dti;
    }
}
