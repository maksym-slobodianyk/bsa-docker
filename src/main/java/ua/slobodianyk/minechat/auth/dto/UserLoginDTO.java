package ua.slobodianyk.minechat.auth.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class UserLoginDTO{
    private String email;
    private String password;
}
